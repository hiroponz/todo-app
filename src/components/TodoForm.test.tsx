import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import TodoForm from './TodoForm';

describe('TodoForm component', () => {
  it('should call the addTodo function with the value when submit is clicked', () => {
    const addTodoMock = jest.fn();
    const { getByPlaceholderText, getByText } = render(<TodoForm addTodo={addTodoMock} />);

    const input = getByPlaceholderText('新しいToDoを追加');
    fireEvent.change(input, { target: { value: 'test todo' } });
    const button = getByText('追加');
    fireEvent.click(button);

    expect(addTodoMock).toBeCalledWith('test todo');
  });
});
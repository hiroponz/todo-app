import { useState, FormEvent } from 'react';

type Props = {
  addTodo: (value: string) => void;
};

const TodoForm: React.FC<Props> = ({ addTodo }) => {
  const [value, setValue] = useState('');

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        placeholder="新しいToDoを追加"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <button type="submit">追加</button>
    </form>
  );
};

export default TodoForm;

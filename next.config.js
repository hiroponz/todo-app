/** @type {import('next').NextConfig} */

/* GitLab Pagesでの公開時のサブディレクトリ */
const subDirectory = process.env.CI_PROJECT_NAME ? "/" + process.env.CI_PROJECT_NAME : "";

const nextConfig = {
  reactStrictMode: true,
  basePath: subDirectory,
  assetPrefix: subDirectory,
  publicRuntimeConfig: {
    basePath: subDirectory,
  },
}

module.exports = nextConfig
